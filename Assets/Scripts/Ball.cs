﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

public Vector2 speed;

private Vector2 iniSpeed;
private Vector3 iniPos;

// Use this for initialization
void Start()
{
    iniSpeed = speed;
    iniPos = transform.position;
}

// Update is called once per frame
void Update()
{
    transform.Translate(speed * Time.deltaTime);
}

private void OnTriggerEnter(Collider other)
{

    switch(other.tag)
    {
            case "Border":
                speed.x = -speed.x;
                break;
            case "Player":
                speed.y = -speed.y;
                break;
            case "Brick":
                other.gameObject.GetComponent<Brick>().Touch();
                speed.y = -speed.y;
                break;
            case "Finish":
                Reset();
                break;    
    }
}

void Reset()
{
    transform.position = iniPos;
    speed = iniSpeed;
}
}
