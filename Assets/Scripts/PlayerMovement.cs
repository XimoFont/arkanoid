﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float velx;
    public float maxx;
    private float moveHorizontal;
    private Vector3 positionActual;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        moveHorizontal = Input.GetAxis("Horizontal");

        positionActual = transform.position;

        if(moveHorizontal > 0)
        {
            positionActual.x = positionActual.x + velx * moveHorizontal;
        }
        if(positionActual.x >= maxx)
        {
            positionActual.x = maxx;
        }
        if(positionActual.x <= -maxx)
        {
            positionActual.x = -maxx;
        }
        else if(moveHorizontal < 0)
        {
            positionActual.x = positionActual.x + velx * moveHorizontal;
        }

        transform.position = positionActual;

    }
}